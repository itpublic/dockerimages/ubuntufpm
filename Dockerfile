FROM ubuntu:jammy

RUN apt-get update \
    && apt-get dist-upgrade -y \
    && apt-get install -y curl wget git openssh-client ruby ruby-dev ruby-rubygems build-essential jq \
    && gem install --no-document fpm \
    && gem install --no-document backports
